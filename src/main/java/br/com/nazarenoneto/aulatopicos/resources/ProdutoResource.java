package br.com.nazarenoneto.aulatopicos.resources;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.nazarenoneto.aulatopicos.business.ProdutoService;
import br.com.nazarenoneto.aulatopicos.model.Produto;

@Stateless
@Path("/produto")
public class ProdutoResource {
	@EJB
	private ProdutoService service;

	@GET
	@Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
	public List<Produto> listagem() {
		return service.listagem();
	}

	@GET
	@Path("/produto/{descricao}")
	public String login(@PathParam("descricao") String descricao) {
		return "Pegando a descri��o: " + descricao;
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
	public Response novo(Produto produto) {
		service.salvarOuAtualizar(produto);
		return Response.status(200).entity(produto).build();
	}

	@Path("/{id}")
	@DELETE
	@Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
	public Response apgar(@PathParam("id") int id) {
		Produto produto = service.findById(id);
		service.apagar(produto);
		return Response.status(200).build();
	}
}
