package br.com.nazarenoneto.aulatopicos.resources;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.nazarenoneto.aulatopicos.business.UsuarioService;
import br.com.nazarenoneto.aulatopicos.model.Usuario;

@Stateless
@Path("/consulta")
public class UsuarioResource {
	@EJB
	private UsuarioService service;

	@GET
	@Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
	@Path("/usuarios")
	public List<Usuario> listagem() {
		return service.listagem();
	}

	@GET
	@Path("/usuarios/{login}")
	public String login(@PathParam("login") String login) {
		return "Pegando o login: " + login;
	}

	@POST
	@Path("/novousuario")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
	public Response novo(Usuario usuario) {
		service.salvarOuAtualizar(usuario);
		return Response.status(200).entity(usuario).build();
	}
}
