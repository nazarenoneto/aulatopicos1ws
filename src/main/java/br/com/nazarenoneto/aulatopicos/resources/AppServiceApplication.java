package br.com.nazarenoneto.aulatopicos.resources;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/services")
public class AppServiceApplication extends Application {

}
