package br.com.nazarenoneto.aulatopicos.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.nazarenoneto.aulatopicos.model.Produto;

@Stateless
public class ProdutoDao {
	@PersistenceContext
	private EntityManager em;

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Produto salvarOuAtualizar(Produto produto) {
		if (produto.getId() == 0) {
			em.persist(produto);
		} else {
			em.merge(produto);
		}
		return produto;
	}

	@SuppressWarnings("unchecked")
	public List<Produto> listagem() {
		List<Produto> lista = (List<Produto>) em.createQuery("select p from Produto p").getResultList();
		if (lista == null) {
			lista = new ArrayList<>();
		}
		return lista;
	}

	public void apagar(Produto produto) {
		em.remove(produto);
	}

	public Produto findById(int id) {
		return em.find(Produto.class, id);
	}
}
