package br.com.nazarenoneto.aulatopicos.business;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.com.nazarenoneto.aulatopicos.dao.ProdutoDao;
import br.com.nazarenoneto.aulatopicos.model.Produto;

@Stateless
public class ProdutoService {
	@Inject
	private ProdutoDao produtoDao;

	public Produto salvarOuAtualizar(Produto produto) {
		produtoDao.salvarOuAtualizar(produto);
		return produto;
	}

	public void apagar(Produto produto) {
		produtoDao.apagar(produto);
	}

	public List<Produto> listagem() {
		return produtoDao.listagem();
	}

	public Produto findById(int id) {
		return produtoDao.findById(id);
	}
}
